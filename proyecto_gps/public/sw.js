// configuracion del service workect
importScripts('js/sw-ultils.js')

const ESTATICO_CACHE = 'estatic-v1'
const DINAMICO_CACHE = 'dinamico-v1'
const INMUTABLE_CACHE = 'inmutable-v1'

const APP_SHELL = [
  '/',
  'index.html',
  'style.css',
  'Holav1.png',
  'js/app.js',
  'js/sw-ultils.js',
]



const APP_SHELL_INMUTABLE = [
  'https://fonts.googleapis.com/css2?family=Roboto:wght@100&display=swap'
]


self.addEventListener("install", (event) => {

  const cacheStatic = caches.open(ESTATICO_CACHE).then(cache =>
    cache.addAll(APP_SHELL))

  const cacheInmutable = caches.open(INMUTABLE_CACHE).then(cache =>
    cache.addAll(APP_SHELL_INMUTABLE))

  event.waitUntil(Promise.all[cacheStatic, cacheInmutable])
});

self.addEventListener("activate", (event) => {
  const respuesta = caches.keys().then(keys => {
    keys.forEach(key => {
      if (key !== ESTATICO_CACHE && key.includes('static')) {
        return caches.delete(key)
      }
    })
  })
  event.waitUntil(respuesta)
});

self.addEventListener("fetch", (event) => {
  const respuesta = caches.match(event.request).then(res => {
    if (res) {
      return res
    } else {
      return fetch(event.request).then(newRes => {
        return actualizadoCacheDinamico(DINAMICO_CACHE, event.request, newRes)
      })
    }
  })
  event.respondWith(respuesta)
});


