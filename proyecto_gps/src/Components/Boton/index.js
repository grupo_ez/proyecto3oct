import * as React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Modal from '@mui/material/Modal';
import AddCommentIcon from '@mui/icons-material/AddComment';
import SendIcon from '@mui/icons-material/Send';
import TextField from '@mui/material/TextField';
import FormControl from '@mui/material/FormControl';



function Boton(props) {
    const [open, setOpen] = React.useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);
    const { handleAddItem } = props;
    const [description, setDescription] = React.useState("");

    const handleSubmit = e => {
        e.preventDefault();
        handleAddItem({
            done: false,
            id: (+new Date()).toString(),
            description
        });


        
        setDescription("");

    };
    return (
        <div>
            <Button onClick={handleOpen}><AddCommentIcon /></Button>
            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <form onSubmit={handleSubmit}>
                        <FormControl  sx={{ width: '100%', display: 'flex', }} variant="standard">
                            <TextField
                                id="addTask"
                                label="Task"
                                placeholder="Agrega una tarea"
                                multiline
                                variant="filled"
                                value={description}
                                onChange={e => setDescription(e.target.value)}
                            />
                            <Button type="submit" disabled={description ? "" : "disabled"} sx={{ top: '10px', }} variant="contained" size="large" endIcon={<SendIcon />}>
                                Send
                            </Button>
                        </FormControl>
                    </form>

                </Box>
            </Modal>
        </div>
    )
}
const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

export { Boton }