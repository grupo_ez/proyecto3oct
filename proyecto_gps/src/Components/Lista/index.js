import React from "react";
// import './Lista.css';
import List from '@mui/material/List';
import Item from '../Item';
import DeleteIcon from '@mui/icons-material/Delete';
import Button from '@mui/material/Button';
import CheckboxTplSvg from "../CheckboxTplSvg";

function Lista(props) {

  const { tareas, setTareas,buscandoValor } = props;

    //Crear el Array vacío
    let buscarTareas = []
    //Condicional del buscador
    if (!buscandoValor.length >= 1) {
      buscarTareas = tareas
    } else {
      buscarTareas = tareas.filter(tarea => {
        const tareaText = tarea.description.toLowerCase()
        const buscarText = buscandoValor.toLowerCase()
        //Una vez convertido en minúscula...
        return tareaText.includes(buscarText)
      })
    }

  const onChangeStatus = e => {
    const { name, checked  } = e.target;

    const updateTareas = tareas.map(item => ({
      ...item,
      done: item.id === name ? checked  : item.done
    }));
    setTareas(updateTareas);
  }
  const removeTask = e => {
    const updateList = tareas.filter(item => !item.done);
    setTareas(updateList);
  };
  //Funcion que mostrara las tareas con un checkbox para completar la tarea
  const checkbox = buscarTareas.map(item => (
    <Item key={item.id} data={item} onChange={onChangeStatus}/>
  ));

  return (
    <List sx={{ width: '80%', maxWidth: '80%', margin: '15px', bgcolor: 'background.paper' }}>
        <CheckboxTplSvg />
        {tareas.length ? checkbox : "No tasks"}
        {tareas.length ? (
          <p>
            <Button variant="outlined" startIcon={<DeleteIcon />} onClick={removeTask}>
              Delete
            </Button>

          </p>
        ) : null}
    </List>
  )

  
}

export { Lista }