import React from "react";
import './Contador.css';

function Contador({total,completed}){
    return(
        <h2 className="tituloContador">Has completado: {completed} de {total} tareas</h2>

    )

}

export {Contador}