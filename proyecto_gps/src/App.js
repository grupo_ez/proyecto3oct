import "./App.css";
import React from "react";
import { Contador } from "./Components/Contador";
import { Buscador } from "./Components/Buscador";
import { Lista } from "./Components/Lista";
import { Boton } from "./Components/Boton";
//import Button from "@mui/material/Button";

function App() {
  
  //C O N T A D O R
  //CRear el estado
  const [tareas, setTareas] = React.useState([])
  //Hacer variables para almacenar tareas completadas y el total de tareas
  const tareaCompleta = tareas.filter(tarea => !!tarea.done).length
  //Variable que almacena el total de tareas
  const totalTareas = tareas.length

  //BUSCADOR 
  //Crear el estado
  const [buscandoValor, setBuscandoValor] = React.useState('')

  const handleAddItem = addItem => {
    setTareas([...tareas, addItem]);
  };

  return (
    <React.Fragment>
      <Buscador buscandoValor={buscandoValor}
        setBuscandoValor={setBuscandoValor}
      />
      <Contador
        total={totalTareas}
        completed={tareaCompleta}

      />
      <Lista tareas={tareas} setTareas={setTareas} buscandoValor={buscandoValor}>
      </Lista>
      <Boton handleAddItem={handleAddItem} />
    </React.Fragment>
  );
}

export default App;
